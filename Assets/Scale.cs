﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale : MonoBehaviour {
	float quadH;
	float quadW;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		quadH = (float)Camera.main.orthographicSize * 2.0F;
		quadW = (float)quadH * ((float)Screen.width / (float)Screen.height);
		//GameObject.Find ("StartBt").transform.localScale = new Vector3 (quadW, quadH, 1.0F);
		GameObject.Find ("Background").transform.localScale = new Vector3 (quadW, quadH, 1.0F);
	}
}
