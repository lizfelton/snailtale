﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DanceController : MonoBehaviour {

    List<GameObject> arrows;

    float left = 200.0F;
    float down = 260.0F;
    float up = 320.0F;
    float right = 380.0F;

    float yScale = 45F;

    bool WaitingForUser = false;

    string[] currentStep;
    string dance;
    string dateName;
    int stepsRead = 0;


    // Use this for initialization
    void Start () {
        arrows = new List<GameObject>();
        dateName = GameObject.Find("GameInfo").GetComponent<GameController>().currentDate.ToString();
	}
	
	// Update is called once per frame
	void Update () {
        if (!WaitingForUser)
        {
            dance = GameObject.Find(dateName).GetComponent<DateControl>().GetNextSequence();
            //Debug.Log(dance.ToString());
            currentStep = dance.Split('/');
            stepsRead = 0;

            GenerateArrows();

            WaitingForUser = true;
        }

        if (dance.Equals("no"))
        {
            Debug.Log("dance is no");
            //end game stuff 

            if (GameObject.Find("GameInfo").GetComponent<GameController>().score > 50)
            {
                if(dateName.Equals("Butch")){
                    SceneManager.LoadScene(14);
                }else if (dateName.Equals("Shelley"))
                {
                    SceneManager.LoadScene(18);
                }
            }
            else
            {
                SceneManager.LoadScene(17);
            }
            
        }

        if (WaitingForUser && stepsRead < currentStep.Length)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                stepsRead++;
                Debug.Log(stepsRead + " of total " + currentStep.Length);
            }
        } else if (stepsRead > currentStep.Length)
        {
            Debug.Log(stepsRead);
            Debug.Log("not waiting");
            WaitingForUser = false;
        }

    }

    public bool GetWaitingForUser()
    {
        return WaitingForUser;
    }

    public void SetWaitingForUser(bool waiting)
    {
        WaitingForUser = waiting;
    }

    void GenerateArrows()
    {
        yScale = 45F;
        for (int i = 0; i < currentStep.Length; i++)
        {
            if (currentStep[i].Equals("up"))
            {
                var arrow = (GameObject)Instantiate(Resources.Load("UpArrowPlay"));
                arrow.name = "up" + i.ToString();
                arrow.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(up, (Screen.height * yScale), 1.7f));
                arrow.gameObject.GetComponent<Renderer>().enabled = true;
                arrows.Add(arrow);
            }else if (currentStep[i].Equals("down"))
            {
                var arrow = (GameObject)Instantiate(Resources.Load("DownArrowPlay"));
                arrow.name = "down" + i.ToString();
                arrow.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(down, (Screen.height * yScale), 1.7f));
                arrow.gameObject.GetComponent<Renderer>().enabled = true;
                arrows.Add(arrow);
            }else if (currentStep[i].Equals("right"))
            {
                var arrow = (GameObject)Instantiate(Resources.Load("RightArrowPlay"));
                arrow.name = "right" + i.ToString();
                arrow.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(right, (Screen.height * yScale), 1.7f));
                arrow.gameObject.GetComponent<Renderer>().enabled = true;
                arrows.Add(arrow);
            }else if (currentStep[i].Equals("left"))
            {
                var arrow = (GameObject)Instantiate(Resources.Load("LeftArrowPlay"));
                arrow.name = "left" + i.ToString();
                arrow.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(left, (Screen.height * yScale), 1.7f));
                arrow.gameObject.GetComponent<Renderer>().enabled = true;
                arrows.Add(arrow);
            }

            yScale -= 30F;
        }
    }
}
