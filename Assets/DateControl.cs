﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;

public class DateControl : MonoBehaviour
{

    public Animator anim;
    int seq = -1;

    string[] danceSteps;

    enum steps { Left, Right, Up, Down };

    int step = 0;

    float beat = 0F;
    int stepNum = 0;

    string[] currentSteps;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();

        try
        {
            anim.SetBool("idle", false);
        }
        catch (Exception e)
        {

        }
        Debug.Log(GameObject.Find("GameInfo").GetComponent<GameController>().currentDate.ToString());
        string fileName = "Assets\\" + GameObject.Find("GameInfo").GetComponent<GameController>().currentDate.ToString() + "Dance.txt";

        var dance = File.ReadAllText(fileName);
        danceSteps = dance.Split('.');
    }

    // Update is called once per frame
    void Update()
    {
        if (currentSteps[step].Equals("up") && beat < 5)
        {
            anim.SetBool("up", true);

            anim.SetBool("down", false);
            anim.SetBool("left", false);
            anim.SetBool("right", false);
        }
        else if (currentSteps[step].Equals("down") && beat < 5)
        {
            anim.SetBool("down", true);

            anim.SetBool("up", false);
            anim.SetBool("left", false);
            anim.SetBool("right", false);
        }
        else if (currentSteps[step].Equals("left") && beat < 5)
        {
            anim.SetBool("left", true);

            anim.SetBool("down", false);
            anim.SetBool("up", false);
            anim.SetBool("right", false);
        }
        else if (currentSteps[step].Equals("right") && beat < 5)
        {
            anim.SetBool("right", true);

            anim.SetBool("down", false);
            anim.SetBool("left", false);
            anim.SetBool("up", false);
        }

        beat += Time.deltaTime;
        if (beat >= 5)
        {
            Debug.Log("changing step");
            if (step == currentSteps.Length)
            {
                step = 0;
            }
            else
            {
                step++;
            }

            beat = 0F;

        }

    }

    public string GetNextSequence()
    {
        seq++;
        if (seq > danceSteps.Length)
        {
            return "no";
        }
        currentSteps = danceSteps[seq].Split('/');
        Debug.Log(danceSteps[seq].ToString());
        return danceSteps[seq];

    }

}
