﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Link : MonoBehaviour
{
	public void LoadByIndex (int sceneIndex)
	{
        SceneManager.LoadScene(sceneIndex);
    }

    public void ExitGame()
    {
        Debug.Log("Game has Ended");
        Application.Quit();
    }
}

