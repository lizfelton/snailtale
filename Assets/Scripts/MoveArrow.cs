﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveArrow : MonoBehaviour {
	GameObject thisArrow;

	float movementPerFrame = 1.35F;

	float yTransform;

	float left = 200.0F;
	float down = 260.0F;
	float up = 320.0F;
	float right = 380.0F;

	float myXPos;

	GameObject boxInWorld;
	GameObject goodBox;
	GameObject badBox;

	int beat = 0;

	KeyCode myKey;

	// Use this for initialization
	void Start () {
		thisArrow = GameObject.Find(gameObject.name);

		yTransform = thisArrow.transform.position.y;

		if (gameObject.name.Contains ("up")) {
			myXPos = up;
			boxInWorld = GameObject.Find ("upBox");
			myKey = KeyCode.UpArrow;
			goodBox = GameObject.Find ("UpGood");
			badBox = GameObject.Find ("UpBad");
			name = "up";
		} else if (gameObject.name.Contains ("down")) {
			myXPos = down;
			boxInWorld = GameObject.Find ("downBox");
			myKey = KeyCode.DownArrow;
			goodBox = GameObject.Find ("DownGood");
			badBox = GameObject.Find ("DownBad");
			name = "down";
		}else if (gameObject.name.Contains ("left")) {
			myXPos = left;
			boxInWorld = GameObject.Find ("leftBox");
			myKey = KeyCode.LeftArrow;
			goodBox = GameObject.Find ("LeftGood");
			badBox = GameObject.Find ("LeftBad");
			name = "left";
		}else if (gameObject.name.Contains ("right")) {
			myXPos = right;
			boxInWorld = GameObject.Find ("rightBox");
			myKey = KeyCode.RightArrow;
			goodBox = GameObject.Find ("RightGood");
			badBox = GameObject.Find ("RightBad");
			name = "right";
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(myKey)){
			if (InBox ().Equals ("good")) {
				boxInWorld.GetComponent<Renderer>().enabled = false;
				goodBox.GetComponent<Renderer>().enabled = true;
				Debug.Log ("good!");
				beat = 0;

			} else if (InBox ().Equals ("fail")) {
				boxInWorld.GetComponent<Renderer>().enabled = false;
				badBox.GetComponent<Renderer>().enabled = true;
				Debug.Log ("fail!");
				beat = 0;

			} else {
				Debug.Log ("oh noes");
			}
		}

		yTransform += movementPerFrame;

		if (yTransform > Screen.height) {
            Destroy(this.gameObject);

		} else {
			thisArrow.transform.position = Camera.main.ScreenToWorldPoint(new Vector3 (myXPos, yTransform, 1.7F));
		}

		if (beat > 5) {
			boxInWorld.GetComponent<Renderer> ().enabled = true;
			goodBox.GetComponent<Renderer> ().enabled = false;
			badBox.GetComponent<Renderer> ().enabled = false;
			beat = -1;
		} else {
			beat++;
		}
			
	}

	string InBox(){
		//float y = Camera.main.WorldToScreenPoint (boxInWorld.transform.position).y;
		if(gameObject.GetComponent<BoxCollider2D>().IsTouching(boxInWorld.GetComponent<BoxCollider2D>())){
			//Debug.Log (Mathf.Abs(yTransform - y));
			Score ("good");
			return "good";
		}else {
			//Debug.Log (Mathf.Abs (yTransform - y));
			Score ("fail");
			return "fail";
		}

	}

	void Score(string hitType){
		var globalScript = GameObject.Find ("GameInfo").GetComponent<GameController> ();
		if (hitType.Equals ("good")) {
			globalScript.score += 10;
		} else if (hitType.Equals ("fail")) {
			globalScript.score -= 10;
		} else {
			globalScript.score -= 20;
		}
	}

	Vector3 GetPosition(){
		return thisArrow.transform.position;
	}
}
