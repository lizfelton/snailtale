﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class run : MonoBehaviour {

	Vector3 newTransform;
	float quadWidth;
	float quadHeight;

	GameObject upArrowGood;
	GameObject downArrowGood;
	GameObject leftArrowGood;
	GameObject rightArrowGood;

	GameObject upArrowBad;
	GameObject downArrowBad;
	GameObject leftArrowBad;
	GameObject rightArrowBad;

	//copy this
	GameObject upArrowPlay;
	GameObject downArrowPlay;
	GameObject leftArrowPlay;
	GameObject rightArrowPlay;

	GameObject upBox;
	GameObject downBox;
	GameObject leftBox;
	GameObject rightBox;

	//copy this
	float left = 200.0F;
	float down = 260.0F;
	float up = 320.0F;
	float right = 380.0F;

	// Use this for initialization
	void Start () {
		quadHeight = (float)Camera.main.orthographicSize * 2.0F;
		quadWidth = (float)quadHeight * ((float)Screen.width / (float)Screen.height);

		GameObject.Find ("Background").transform.localScale = new Vector3 (quadWidth, quadHeight, 1.0F);

		upBox = (GameObject)Instantiate (Resources.Load ("UpArrow"));
		downBox = (GameObject)Instantiate (Resources.Load ("DownArrow"));
		leftBox = (GameObject)Instantiate (Resources.Load ("LeftArrow"));
		rightBox = (GameObject)Instantiate (Resources.Load ("RightArrow"));

		upArrowGood = (GameObject)Instantiate (Resources.Load ("UpGood"));
		upArrowGood.gameObject.name = "UpGood";
		downArrowGood = (GameObject)Instantiate (Resources.Load ("DownGood"));
		downArrowGood.gameObject.name = "DownGood";
		leftArrowGood = (GameObject)Instantiate (Resources.Load ("LeftGood"));
		leftArrowGood.gameObject.name = "LeftGood";
		rightArrowGood = (GameObject)Instantiate (Resources.Load ("RightGood"));
		rightArrowGood.gameObject.name = "RightGood";

		upArrowBad = (GameObject)Instantiate (Resources.Load ("UpBad"));
		upArrowBad.gameObject.name = "UpBad";
		downArrowBad = (GameObject)Instantiate (Resources.Load ("DownBad"));
		downArrowBad.gameObject.name = "DownBad";
		leftArrowBad = (GameObject)Instantiate (Resources.Load ("LeftBad"));
		leftArrowBad.gameObject.name = "LeftBad";
		rightArrowBad = (GameObject)Instantiate (Resources.Load ("RightBad"));
		rightArrowBad.gameObject.name = "RightBad";

		upArrowGood.GetComponent<Renderer>().enabled = false;
		downArrowGood.GetComponent<Renderer>().enabled = false;
		leftArrowGood.GetComponent<Renderer>().enabled = false;
		rightArrowGood.GetComponent<Renderer>().enabled = false;

		upArrowBad.GetComponent<Renderer>().enabled = false;
		downArrowBad.GetComponent<Renderer>().enabled = false;
		leftArrowBad.GetComponent<Renderer>().enabled = false;
		rightArrowBad.GetComponent<Renderer>().enabled = false;

		var upPos = Camera.main.ScreenToWorldPoint(new Vector3 (up, 600.0F, 1.8F));
		var downPos = Camera.main.ScreenToWorldPoint(new Vector3 (down, 600.0F, 1.8F));
		var leftPos = Camera.main.ScreenToWorldPoint(new Vector3 (left, 600.0F, 1.8F));
		var rightPos = Camera.main.ScreenToWorldPoint(new Vector3 (right, 600.0F, 1.8F));

		upArrowGood.transform.position = upPos;
		downArrowGood.transform.position = downPos;
		leftArrowGood.transform.position = leftPos;
		rightArrowGood.transform.position = rightPos;

		upArrowBad.transform.position = upPos;
		downArrowBad.transform.position = downPos;
		leftArrowBad.transform.position = leftPos;
		rightArrowBad.transform.position = rightPos;

		upBox.transform.position = upPos;
		upBox.gameObject.name = "upBox";
		downBox.transform.position = downPos;
		downBox.gameObject.name = "downBox";
		leftBox.transform.position = leftPos;
		leftBox.gameObject.name = "leftBox";
		rightBox.transform.position = rightPos;
		rightBox.gameObject.name = "rightBox";


		/*upArrowPlay = (GameObject)Instantiate (Resources.Load ("UpArrowPlay"));
		downArrowPlay = (GameObject)Instantiate (Resources.Load ("DownArrowPlay"));
		leftArrowPlay = (GameObject)Instantiate (Resources.Load ("LeftArrowPlay"));
		rightArrowPlay = (GameObject)Instantiate (Resources.Load ("RightArrowPlay"));

		var startupPos = Camera.main.ScreenToWorldPoint(new Vector3 (up, (Screen.height*45F), 1.7F));
		var startdownPos = Camera.main.ScreenToWorldPoint(new Vector3 (down, (Screen.height*45F), 1.7F));
		var startleftPos = Camera.main.ScreenToWorldPoint(new Vector3 (left, (Screen.height*45F), 1.7F));
		var startrightPos = Camera.main.ScreenToWorldPoint(new Vector3 (right, (Screen.height*45F), 1.7F));

		upArrowPlay.transform.position = startupPos;
		downArrowPlay.transform.position = startdownPos;
		leftArrowPlay.transform.position = startleftPos;
		rightArrowPlay.transform.position = startrightPos;*/
	}
	
	// Update is called once per frame
	void Update () {

	}
}
