﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewBehaviourScript : MonoBehaviour {

    public Image ChancesRemaining;

    private float chances = 10;
    private float maxChances = 10;
	private float penalty = 1;

    private void Start()
    {
        UpdateChancesRemaining();
    }

    private void UpdateChancesRemaining()
    {
        float ratio = chances / maxChances;
        ChancesRemaining.rectTransform.localScale = new Vector3(ratio, 1, 1);
    }

    private void TakeDamage(float mistake)
    {
        chances -= penalty;
        if(chances <= 0)
        {
            chances = 0;
            Debug.Log("Salty Snail...");
        }

        UpdateChancesRemaining();
    }
}
