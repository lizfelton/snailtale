﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class energyRun : MonoBehaviour {

	GameObject upArrowPlay;
	GameObject downArrowPlay;
	GameObject leftArrowPlay;
	GameObject rightArrowPlay;

	float left = 200.0F;
	float down = 260.0F;
	float up = 320.0F;
	float right = 380.0F;

	//creating arrow arrays
	public Queue<GameObject> upArrows = new Queue<GameObject>();
	public Queue<GameObject> downArrows = new Queue<GameObject>();
	public Queue<GameObject> leftArrows = new Queue<GameObject>();
	public Queue<GameObject> rightArrows = new Queue<GameObject>();

	// Use this for initialization
	void Start () {
		
		//up array for loop
		for (int i = 0; i < 70; i++) {
			upArrowPlay = (GameObject)Instantiate (Resources.Load ("UpArrowPlay"));
			upArrowPlay.gameObject.name = "up"+i;
			//startupPos = Camera.main.ScreenToWorldPoint(new Vector3 (up, 1.0F, 1.7F));
			upArrowPlay.transform.position = (new Vector3 (up, 10000F, 1.7F));
			upArrowPlay.gameObject.GetComponent<Renderer> ().enabled = false;
			upArrows.Enqueue(upArrowPlay);
		}

		//down array for loop
		for (int i = 0; i < 70; i++) {
			downArrowPlay = (GameObject)Instantiate (Resources.Load ("DownArrowPlay"));
			downArrowPlay.gameObject.name = "down"+i;
			//startdownPos = Camera.main.ScreenToWorldPoint(new Vector3 (down, 1.0F, 1.7F));
			downArrowPlay.transform.position = (new Vector3 (down, 10000F, 1.7F));
			downArrowPlay.gameObject.GetComponent<Renderer> ().enabled = false;
			downArrows.Enqueue(downArrowPlay);
		}

		//left array for loop
		for (int i = 0; i < 70; i++) {
			leftArrowPlay = (GameObject)Instantiate (Resources.Load ("LeftArrowPlay"));
			leftArrowPlay.gameObject.name = "left"+i;
			//startleftPos = Camera.main.ScreenToWorldPoint(new Vector3 (left, 1.0F, 1.7F));
			leftArrowPlay.transform.position = (new Vector3 (left, 10000F, 1.7F));
			leftArrowPlay.gameObject.GetComponent<Renderer> ().enabled = false;
			leftArrows.Enqueue(leftArrowPlay);
		}

		//right array for loop
		for (int i = 0; i < 70; i++) {
			var rightArrowPlay = (GameObject)Instantiate (Resources.Load ("RightArrowPlay"));
			rightArrowPlay.gameObject.name = "right"+i;
			//startrightPos = Camera.main.ScreenToWorldPoint(new Vector3 (right, 1.0F, 1.7F));
			rightArrowPlay.transform.position = (new Vector3 (right, 10000F, 1.7F));
			rightArrowPlay.gameObject.GetComponent<Renderer> ().enabled = false;
			rightArrows.Enqueue(rightArrowPlay);
		}

		float yScale = 45F;

		//1st set
		StartCoroutine(sequence1(yScale));
		StartCoroutine(sequence1(yScale - 30F));

		//2nd set
		StartCoroutine(sequence2(yScale - 60F));
		StartCoroutine(sequence2(yScale - 90F));

		//3rd set
		StartCoroutine(sequence3(yScale - 120F));
		StartCoroutine(sequence3(yScale - 150F));

		//4th set
		StartCoroutine(sequence4(yScale - 180F));
		StartCoroutine(sequence4(yScale - 210F));
		StartCoroutine(sequence6(yScale - 240F));
		StartCoroutine(sequence6(yScale - 270F));
		StartCoroutine(sequence4(yScale - 300F));
		StartCoroutine(sequence4(yScale - 330F));
		StartCoroutine(sequence3(yScale - 360F));
		StartCoroutine(sequence3(yScale - 390F));
		StartCoroutine(sequence4(yScale - 420F));
		StartCoroutine(sequence4(yScale - 450F));

		//5th set
		StartCoroutine(sequence5(yScale - 480F));
		StartCoroutine(sequence5(yScale - 510F));


		float timeWaited = 0.0F;
		while (timeWaited < 2000.0F) {
			timeWaited += Time.deltaTime;
		}

		//SceneManager.LoadScene (13);

		/*6th set
		StartCoroutine(sequence6(yScale - 450F));
		StartCoroutine(sequence6(yScale - 475F));
		StartCoroutine(sequence6(yScale - 500F));
		StartCoroutine(sequence6(yScale - 525F));*/

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//1st set of steps/notes (in order of appearance) played 2 times
	IEnumerator sequence1 (float yScale) {
		var leftArrow = leftArrows.Dequeue ();
		leftArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (left, (Screen.height*yScale), 1.7f));
		leftArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var rightArrow = rightArrows.Dequeue ();
		rightArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (right, (Screen.height*yScale)-2000f, 1.7f));
		rightArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		leftArrow = leftArrows.Dequeue ();
		leftArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (left, (Screen.height*yScale)-5000f, 1.7f));
		leftArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		rightArrow = rightArrows.Dequeue ();
		rightArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (right, (Screen.height*yScale)-6000f, 1.7f));
		rightArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		leftArrow = leftArrows.Dequeue ();
		leftArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (left, (Screen.height*yScale)-7000f, 1.7f));
		leftArrow.gameObject.GetComponent<Renderer> ().enabled = true;
		yield return new WaitForSecondsRealtime (5);
	}

	//2nd set of steps/notes (in order of appearance) played 2 times
	IEnumerator sequence2 (float yScale) {
		var upArrow = upArrows.Dequeue ();
		upArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (up, (Screen.height*yScale), 1.7f));
		upArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var downArrow = downArrows.Dequeue ();
		downArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (down, (Screen.height*yScale)-2000f, 1.7f));
		downArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		upArrow = upArrows.Dequeue ();
		upArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (up, (Screen.height*yScale)-5000f, 1.7f));
		upArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		downArrow = downArrows.Dequeue ();
		downArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (down, (Screen.height*yScale)-6000f, 1.7f));
		downArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		upArrow = upArrows.Dequeue ();
		upArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (up, (Screen.height*yScale)-7000f, 1.7f));
		upArrow.gameObject.GetComponent<Renderer> ().enabled = true;
		yield return new WaitForSecondsRealtime (5);
	}

	//3rd set of steps/notes (in order of appearance) played 2 times
	IEnumerator sequence3 (float yScale) {
		var leftArrow = leftArrows.Dequeue ();
		leftArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (left, (Screen.height*yScale), 1.7f));
		leftArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var downArrow = downArrows.Dequeue ();
		downArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (down, (Screen.height*yScale)-2000f, 1.7f));
		downArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var upArrow = upArrows.Dequeue ();
		upArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (up, (Screen.height*yScale)-5000f, 1.7f));
		upArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var rightArrow = rightArrows.Dequeue ();
		rightArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (right, (Screen.height*yScale)-6000f, 1.7f));
		rightArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		upArrow = upArrows.Dequeue ();
		upArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (up, (Screen.height*yScale)-7000f, 1.7f));
		upArrow.gameObject.GetComponent<Renderer> ().enabled = true;
		yield return new WaitForSecondsRealtime (5);
	}
	
	//4th set of steps/notes (in order of appearance) played 10 times
	IEnumerator sequence4(float yScale) {
		var rightArrow = rightArrows.Dequeue ();
		rightArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (right, (Screen.height*yScale), 1.7f));
		rightArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var leftArrow = leftArrows.Dequeue ();
		leftArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (left, (Screen.height*yScale)-2000f, 1.7f));
		leftArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var downArrow = downArrows.Dequeue ();
		downArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (down, (Screen.height*yScale)-5000f, 1.7f));
		downArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var upArrow = upArrows.Dequeue ();
		upArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (up, (Screen.height*yScale)-6000f, 1.7f));
		upArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		downArrow = downArrows.Dequeue ();
		downArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (down, (Screen.height*yScale)-7000f, 1.7f));
		downArrow.gameObject.GetComponent<Renderer> ().enabled = true;
		yield return new WaitForSecondsRealtime (5);
	}

	//5th set of steps/notes (in order of appearance) played 2 times
	IEnumerator sequence5(float yScale) {
		var rightArrow = rightArrows.Dequeue ();
		rightArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (right, (Screen.height*yScale), 1.7f));
		rightArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var leftArrow = leftArrows.Dequeue ();
		leftArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (left, (Screen.height*yScale)-1000f, 1.7f));
		leftArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		rightArrow = rightArrows.Dequeue ();
		rightArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (right, (Screen.height*yScale)-2000f, 1.7f));
		rightArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		leftArrow = leftArrows.Dequeue ();
		leftArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (left, (Screen.height*yScale)-3000f, 1.7f));
		leftArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var downArrow = downArrows.Dequeue ();
		downArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (down, (Screen.height*yScale)-4000f, 1.7f));
		downArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var upArrow = upArrows.Dequeue ();
		upArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (up, (Screen.height*yScale)-5000f, 1.7f));
		upArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		downArrow = downArrows.Dequeue ();
		downArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (down, (Screen.height*yScale)-6000f, 1.7f));
		downArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		upArrow = upArrows.Dequeue ();
		upArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (up, (Screen.height*yScale)-7000f, 1.7f));
		upArrow.gameObject.GetComponent<Renderer> ().enabled = true;
		yield return new WaitForSecondsRealtime (5);
	}

	IEnumerator sequence6(float yScale) {
		var downArrow = downArrows.Dequeue ();
		downArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (down, (Screen.height*yScale), 1.7f));
		downArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var leftArrow = leftArrows.Dequeue ();
		leftArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (left, (Screen.height*yScale)-2000f, 1.7f));
		leftArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var upArrow = upArrows.Dequeue ();
		upArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (up, (Screen.height*yScale)-5000f, 1.7f));
		upArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		var rightArrow = rightArrows.Dequeue ();
		rightArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (right, (Screen.height*yScale)-6000f, 1.7f));
		rightArrow.gameObject.GetComponent<Renderer> ().enabled = true;

		downArrow = downArrows.Dequeue ();
		downArrow.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (down, (Screen.height*yScale)-7000f, 1.7f));
		downArrow.gameObject.GetComponent<Renderer> ().enabled = true;
		yield return new WaitForSecondsRealtime (5);
	}
}
